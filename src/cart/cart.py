from django.conf import settings
from shop.models import Product
from decimal import Decimal


class Cart(object):

    def __init__(self, request):
        """
        Initializes the cart.
        """
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_KEY)
        if not cart:
            # cart is None meaning session contains no cart
            # save an empty cart in the session
            cart = self.session[settings.CART_SESSION_KEY] = {}
        self.cart = cart
    

    def add(self, product: Product, quantity=1, override_quantity=False):
        """
        Add a product to the cart or update its quantity and updates the session info

        Args:
            product (Product): product instance to add or update the cart
            quantity (int, optional): quantity of product to be added. Defaults to 1.
            override_quantity (bool, optional): true to override the quantity. Defaults to False.
        """
        product_id = str(product.id) # session uses JSON. That is why we convert str because JSON allows keys only to be string
        
        if product_id not in self.cart:
            self.cart[product_id] = {
                'quantity': 0,
                'price': str(product.price)
            }
        
        if override_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        
        self.save()

    def remove(self, product: Product):
        """
        Removes the product from the cart and updates the session info

        Args:
            product (Product): product instance to remove from the cart
        """
        product_id = str(product.id)
        
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def __iter__(self):
        """
        Iterate over the items in the cart and get the products from the database.
        
        Starting, we have a cart dictionary at our hand like the following keys:
        self.cart = {
            product_id: {
                "quantity",
                "price"
            }
        }
        """
        product_ids = self.cart.keys()
        # get the query set with products
        products = Product.objects.filter(id__in=product_ids)

        cart = self.cart.copy()
        # add product instance to the dictionary for each product_id
        for product in products:
            cart[str(product.id)]['product'] = product
        """
        with this operation cart copied dict will take the form
        cart = {
            'product_id': {
                'quantity':
                'price':
                'product': Product
            }
            ...
        }
        """

        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item.get('price') * item.get('quantity')
            yield item
        """
        With this operation cart dict will take the form
        cart = {
            'product_id' : {
                'quantity': int,
                'price': Decimal,
                'product': Product,
                'total_price': Decimal
            }
        }
        """
        
    def __len__(self):
        """
        Count all items (product*quantity) in the cart
        Returns:
            int: total number of items in the cart
        """
        return sum(item['quantity'] for item in self.cart.values())

    def get_total_price(self) -> Decimal:
        """
        Returns the total cost of the cart

        Returns:
            Decimal: total cost of the cart
        """
        return Decimal(sum(Decimal(item['price']) * item.get('quantity') for item in self.cart.values()))
        # return sum(item.get('total_price') for item in self.cart)

    def clear(self):
        """
        Clear the cart session
        """
        del self.session[settings.CART_SESSION_KEY]
        self.save()

    def save(self):
        """
        Marks the session as "modified" to make sure it gets saved
        """
        self.session.modified = True
