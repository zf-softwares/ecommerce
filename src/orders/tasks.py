from celery import shared_task
from django.core.mail import send_mail
from .models import Order


@shared_task
def order_created(order_id: int):
    """
    Task to send an e-mail notification when an order
    is successfully created.

    Args:
        order_id (int): order_id
    """
    order = Order.objects.get(id=order_id)
    subject = f'Order number: {order.id}'
    message = f'Dear {order.first_name}, \n\n' \
                'You have successfully placed an order.'\
                f'Your order ID is {order.id}.'
    mail_sent = send_mail(
        subject=subject,
        message=message,
        recipient_list=[order.email],
        from_email='zfshop@zf.com'
    )
    
    return mail_sent

