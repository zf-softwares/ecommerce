from django.shortcuts import render
from .models import OrderItem, Order
from .forms import OrderCreateForm
from cart.cart import Cart
from .tasks import order_created


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            # create and save an instance of Order class
            order = form.save()
            # add each item of cart to the order and clear the cart
            for item in cart:
                OrderItem.objects.create(
                    order=order,
                    product=item.get('product'),
                    price=item.get('price'),
                    quantity=item.get('quantity')
                    )
                # clear the cart
                cart.clear()
                # launch async task with celery
                order_created.delay(order.id)


                return render(request,
                                'orders/order/created.html',
                                {'order': order})

    # get request
    else:
        form = OrderCreateForm()
    return render(request,
                    'orders/order/create.html',
                    {'cart': cart, 'form': form})