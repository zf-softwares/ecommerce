import random
from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from .models import Category, Product
from django.core.paginator import (
    Paginator, PageNotAnInteger, EmptyPage)
from cart.forms import CartAddProductForm
from threading import Thread


def product_list(request, category_slug=None):

    object_list = Product.availables.all()
    categories = Category.objects.all()
    category = None

    # filtering by category
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        object_list = object_list.filter(category=category)
    
    # filtering by a search_str
    search_str = None
    search_str = request.GET.get('search_str')
    if search_str:
        # # first method
        # object_list = Product.search_by_name_or_description(object_list, search_str)

        # second method
        object_list = object_list.filter(
            Q(name__icontains=search_str) | Q(description__icontains=search_str)
        )
    # sorting
    sort_by: str = request.GET.get('sort_by')
    sorted_by = None
    if sort_by == 'lowest_price':
        object_list = object_list.order_by('price')
        sorted_by = 'Lowest price'
    elif sort_by == 'highest_price':
        object_list = object_list.order_by('-price')
        sorted_by = 'Highest price'
    elif sort_by == 'newest':
        object_list = object_list.order_by('-created')
    elif sort_by == 'oldest':
        object_list = object_list.order_by('created')
    else:
        object_list = object_list.order_by('-created')

    page_size = 12
    paginator = Paginator(object_list, per_page=page_size)
    page_number = request.GET.get('p')

    try:
        products = paginator.get_page(page_number)
    except PageNotAnInteger:
         # If page is not an integer deliver the first page
         products = paginator.get_page(1)
    except EmptyPage:
        # If page is out of range deliver the last page of results
        products = paginator.get_page(paginator.num_pages)

    return render(request,
                    'shop/product/list.html', context={
                        'products': products,
                        'category': category,
                        'categories': categories,
                        'page_number': page_number,
                        'range': range(1, paginator.num_pages+1),
                        'sorted_by': sorted_by,
                        'search_str': search_str})

def product_detail(request, id, slug):
    product = get_object_or_404(Product.availables, id=id, slug=slug)
    cart_product_form = CartAddProductForm()

    return render(request, 'shop/product/detail.html', {
        'product': product,
        'cart_product_form': cart_product_form,
        })
