from django.contrib import admin
from .models import Category, Product


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin): # class name should be of <class_name>Admin
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)} # to auto set the slug field equivalent to name field


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated']
    list_editable = ['price', 'available'] # to edit the fields from the listview
    prepopulated_fields = {'slug': ('name',)} # to auto set the slug field equivalent to name field