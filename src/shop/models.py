from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from django.shortcuts import reverse
from PIL import Image


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])


class AvailableProductsManager(models.Manager):
    
    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(available=True)


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products')
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(default='products/no_img.jpg', upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = models.Manager() #default manager
    availables = AvailableProductsManager() # custom manager

    class Meta:
        ordering = ('name', )
        index_together = (('id', 'slug'), ) # planning to query by both id and slug
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name
    
    @classmethod
    def search_by_name_or_description(cls, query_set: QuerySet,  query_str: str) -> QuerySet:
        products = query_set.filter(
            Q(name__icontains=query_str) | Q(description__icontains=query_str)
        )
        return products
    
    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.id, self.slug])

    def save(self):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 600 or img.width > 600:
            output_size = (600, 600)
            img.thumbnail(output_size)
            img.save(self.image.path)


